#!/bin/sh

LANG=C
export LANG

# testsuite_report_test_run_start <test-name>
testsuite_report_test_failure() {
	echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${1} RESULT=fail>"
}

# testsuite_report_test_run_end <test-name>
testsuite_report_test_skipped() {
	echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${1} RESULT=skip>"
}

# testsuite_report_test_pass <test-name>
testsuite_report_test_pass() {
	echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${1} RESULT=pass>"
}

# testsuite_report_test_data <test-name> [measurement [unit]]
testsuite_report_test_data() {
	if [ -z "$3" ]; then
		if [ -z "$2" ]; then
			echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${1} RESULT=pass>"
		else
			echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${1} RESULT=pass MEASUREMENT=${2}>"
		fi
	else
			echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${1} RESULT=pass MEASUREMENT=${2} UNITS=${3}>"
	fi
}

# testsuite_report_start_case <test-name>
testsuite_report_start_case() {
	echo "<LAVA_SIGNAL_STARTTC ${1}>"
}

# testsuite_report_end_case <test-name>
testsuite_report_end_case() {
	echo "<LAVA_SIGNAL_ENDTC ${1}>"
}

# testsuite_report_end_case_status <test-name>
testsuite_report_end_case_status() {
	STATUS=$?
	testsuite_report_end_case $1
	if [ $STATUS -gt 0 ]; then
		testsuite_report_test_failure $1
		return 1
	else
		testsuite_report_test_pass $1
		return 0
	fi
}

# testsuite_report_end_case_pass <test-name>
testsuite_report_end_case_pass() {
	testsuite_report_end_case $1
	testsuite_report_test_pass $1
}

# testsuite_report_end_case_failure <test-name>
testsuite_report_end_case_failure() {
	testsuite_report_end_case $1
	testsuite_report_test_failure $1
}

# testsuite_check_skip <test-name>
testsuite_check_skip() {
	if [ -n "$ci_testsuite_skip" ]; then
		for skip in $ci_testsuite_skip; do
			if [ "$skip" == "$1" ]; then
				testsuite_report_test_skipped $1
				return 0
			fi
		done
	fi
	return 1
}

# testsuite_check_allow <test-name>
testsuite_check_allow() {
	if [ -n "$ci_testsuite_allow" ]; then
		for allow in $ci_testsuite_allow; do
			if [ "$allow" == "$1" ]; then
				return 0
			fi
		done
	fi
	testsuite_report_test_skipped $1
	return 1
}

