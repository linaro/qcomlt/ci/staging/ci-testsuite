#!/bin/sh

source /usr/lib/ci-testsuite/utils/helpers.sh

command -v mybw > /dev/null 2>&1
if [ $? -gt 0 ]; then
	testsuite_report_test_skipped $1
	exit 0
fi

# No set -e, we handle exec failure ourselves

# MYBW test

testsuite_report_start_case $1

# switch all CPUs in performance
command -v cpufreq-set > /dev/null 2>&1
if [ $? -lt 1 ]; then
	for cpu in $(seq 0 $(($(nproc --all) - 1))); do
		testsuite_report_start_case "$1-cpufreq-$cpu-set-performance"
		cpufreq-set -c $cpu -g performance
		testsuite_report_end_case_status "$1-cpufreq-$cpu-set-performance"
	done
fi

# Only test on cpufreq groups using cpufreq/affected_cpus
# no need to test each cpus
cpu=0
while [ -e /sys/bus/cpu/devices/cpu$cpu ]; do
	testsuite_report_start_case "$1-cpu${cpu}"
	taskset -c $cpu mybw > /tmp/out.log
	testsuite_report_end_case_status "$1-cpu${cpu}"
	if [ $? -lt 1 ]; then
		while read -r line; do
			width=$(echo $line | cut -d":" -f1)
			metric=$(echo $line | cut -d":" -f2 | cut -dM -f1)
			testsuite_report_test_data "$1-cpu${cpu}-${width}" ${metric} "MB/s"
		done < /tmp/out.log
	fi
	last=`cat /sys/bus/cpu/devices/cpu$cpu/cpufreq/affected_cpus | tr " " "\n" | sort | tail -n1`
	cpu=`expr $last + 1`
done

testsuite_report_end_case_pass $1

exit 0
