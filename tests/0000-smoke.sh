#!/bin/sh

source /usr/lib/ci-testsuite/utils/helpers.sh

# SMOKE test

testsuite_report_start_case $1

testsuite_report_start_case "$1-uname"
uname -a
testsuite_report_end_case_status "$1-uname"
testsuite_report_start_case "$1-df"
df -h
testsuite_report_end_case_status "$1-df"
testsuite_report_start_case "$1-free"
free -M
testsuite_report_end_case_status "$1-free"
testsuite_report_start_case "$1-ls-dev"
ls -l /dev | wc -l
testsuite_report_end_case_status "$1-ls-dev"
testsuite_report_start_case "$1-ls-sys"
ls -l /sys | wc -l
testsuite_report_end_case_status "$1-ls-sys"
testsuite_report_start_case "$1-ls-proc"
ls -l /proc | wc -l
testsuite_report_end_case_status "$1-ls-proc"
testsuite_report_start_case "$1-find-lib-modules-ko"
find /lib/modules/ -name "*.ko" | wc -l
testsuite_report_end_case_status "$1-find-lib-modules-ko"
testsuite_report_start_case "$1-lsmod"
lsmod | wc -l
testsuite_report_end_case_status "$1-lsmod"
testsuite_report_start_case "$1-lspci"
lspci
testsuite_report_end_case_status "$1-lspci"
if [ -e /sys/kernel/debug/devices_deferred ]; then
	testsuite_report_start_case "$1-devices_deferred"
	cat /sys/kernel/debug/devices_deferred
	testsuite_report_end_case_status "$1-devices_deferred"
fi
if [ -e /sys/kernel/debug/qcom_socinfo/pmic_model_array ]; then
	testsuite_report_start_case "$1-pmic_model_array"
	cat /sys/kernel/debug/qcom_socinfo/pmic_model_array
	testsuite_report_end_case_status "$1-pmic_model_array"
fi

testsuite_report_end_case_pass $1

exit 0
