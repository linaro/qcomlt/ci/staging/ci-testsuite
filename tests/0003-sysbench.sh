#!/bin/sh

# Ported from test-definitions.git/tree/automated/linux/sysbench/sysbench.sh

source /usr/lib/ci-testsuite/utils/helpers.sh

command -v sysbench > /dev/null 2>&1
if [ $? -gt 0 ]; then
	testsuite_report_test_skipped $1
	exit 0
fi

# This test must be enabled in ci_testsuite_allow kernel params
if ! testsuite_check_allow $1; then
	exit 0
fi

sysbench_log_parser() {
	ms=$(grep -m 1 "total time" "$2" | awk '{print substr($NF,1,length($NF)-1)}')
	testsuite_report_test_data "$1-total-time" "${ms}" "s"

	ms=$(grep "total number of events" "$2" | awk '{print $NF}')
	testsuite_report_test_data "$1-total-number-of-events" "${ms}" "times"

	for i in min avg max; do
		ms=$(grep -m 1 "$i:" "$2" | awk '{print $NF}')
		testsuite_report_test_data "$1-latency-$i" "${ms}" "ms"
	done

	ms=$(grep "events (avg/stddev)" "$2" |  awk '{print $NF}')
	ms_avg=$(echo "${ms}" | awk -F'/' '{print $1}')
	ms_stddev=$(echo "${ms}" | awk -F'/' '{print $2}')
	testsuite_report_test_data "$1-events-avg" "${ms_avg}" "times"
	testsuite_report_test_data "$1-events-stddev" "${ms_stddev}" "times"

	ms=$(grep "execution time (avg/stddev)" "$2" |  awk '{print $NF}')
	ms_avg=$(echo "${ms}" | awk -F'/' '{print $1}')
	ms_stddev=$(echo "${ms}" | awk -F'/' '{print $2}')
	testsuite_report_test_data "$1-execution-time-avg" "${ms_avg}" "s"
	testsuite_report_test_data "$1-execution-time-stddev" "${ms_stddev}" "s"
}

testsuite_report_start_case $1

# CPU
processor_id="$(awk '/^processor/{print $3}' /proc/cpuinfo)"
for cpu in ${processor_id}; do
	testsuite_report_start_case "$1-cpu${cpu}"
	taskset -c "$cpu" sysbench --threads=1 cpu run > /tmp/out.log
	if testsuite_report_end_case_status "$1-cpu${cpu}"; then
		sysbench_log_parser "$1-cpu${cpu}" /tmp/out.log
		ms=$(grep "events per second" /tmp/out.log | awk '{print substr($4,1)}')
		testsuite_report_test_data "$1-cpu${cpu}-events" "${ms}" "events/s"
	fi
done

# Threads, Mutex
for test_type in threads mutex; do
	testsuite_report_start_case "$1-${test_type}"
	sysbench --threads=`nproc` $test_type run > /tmp/out.log
	if testsuite_report_end_case_status "$1-${test_type}"; then
		sysbench_log_parser "$1-${test_type}" /tmp/out.log
	fi
done

# Memory
testsuite_report_start_case "$1-memory"
sysbench --threads=`nproc` memory run > /tmp/out.log
if testsuite_report_end_case_status "$1-memory"; then
	sysbench_log_parser "$1-memory" /tmp/out.log

	ms=$(grep "Total operations" /tmp/out.log | awk '{print substr($4,2)}')
	testsuite_report_test_data "$1-memory-ops" "${ms}" "ops/s"

	ms=$(grep "transferred" /tmp/out.log | awk '{print substr($4, 2)}')
	units=$(grep "transferred" /tmp/out.log | awk '{print substr($5,1,length($NF)-1)}')
	testsuite_report_test_data "$1-memory-transfer" "${ms}" "${units}"
fi

testsuite_report_end_case_pass $1

exit 0
